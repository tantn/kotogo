export interface ExceptionError {
  [key: string]: string[];
}
