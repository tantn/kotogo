import { BadRequestException } from '@nestjs/common';
import { ValidationError } from 'class-validator';
import { ExceptionError } from './interface/exceptionError.interface';

export const ExceptionFactory = (errors: ValidationError[]) => {
  const mewErrors: ExceptionError = {};
  errors.forEach((error) => {
    generateValidation(error, mewErrors);
  });
  throw new BadRequestException({ message: mewErrors });
};

const generateValidation = (error: ValidationError, mewErrors: ExceptionError, parent?: string) => {
  if (error.constraints) {
    mewErrors[parent ? `${parent}.${error.property}` : error.property] = Object.values(
      error.constraints,
    );
  }
  if (error.children.length > 0) {
    error.children.forEach((item) =>
      generateValidation(item, mewErrors, parent ? `${parent}.${error.property}` : error.property),
    );
  }
};
